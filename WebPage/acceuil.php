<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foundation for Sites</title>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>


        <!-- Start Top Bar -->
        <div class="top-bar">
          <div class="top-bar-left">
            <ul class="menu">
              <li class="menu-text">Accueil</li>
            </ul>
          </div>
          <div class="top-bar-right">
            <ul class="menu">
              <li><a href="#">Contact</a></li>
              <li><a href="#">Tarif</a></li>
              <li><a href="#">About</a></li>
            </ul>
          </div>
        </div>
        <!-- End Top Bar -->

        <div class="callout large">
          <div class="row column text-center">
            <h1>Bienvenue au grand Grand Hôtel </h1>
            <p class="lead">Une situation exceptionnelle sur la plage de Trestraou, une des plus belles plages de sable blanc de Tours, vous profitez d’un cadre magnifique qui invite à la contemplation.</p>
            <a href="reserver.html" class="button large">Reserver</a>
            <a href="#" class="button large hollow">About</a>
          </div>
        </div>





        <center><h3>Reservation</h3></center>
            <form role="form" class="wowload fadeInRight">
                 <div class="form-group">
                    <input type="text" class="form-control"  placeholder="Nom">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control"  placeholder="Email">
                </div>
              <center><div class="form-group">
                    <input type="Phone" class="form-control"  placeholder="Phone"></center>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6">
                    <select class="form-control">
                      <option>No. of Adult</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    </div></div>
                </div>
                <div class="form-group">
                    <div class="row">
                    <div class="col-xs-4">
                      <select class="form-control col-sm-2" name="expiry-month" id="expiry-month">
                        <option>Date</option>
                        <option value="01">1</option>
                        <option value="02">2</option>
                        <option value="03">Mar (03)</option>
                        <option value="04">Apr (04)</option>
                        <option value="05">May (05)</option>
                        <option value="06">June (06)</option>
                        <option value="07">July (07)</option>
                        <option value="08">Aug (08)</option>
                        <option value="09">Sep (09)</option>
                        <option value="10">Oct (10)</option>
                        <option value="11">Nov (11)</option>
                        <option value="12">Dec (12)</option>
                      </select>
                    </div>
                    <div class="col-xs-4">
                      <select class="form-control col-sm-2" name="expiry-month" id="expiry-month">
                        <option>Month</option>
                        <option value="01">Jan (01)</option>
                        <option value="02">Feb (02)</option>
                        <option value="03">Mar (03)</option>
                        <option value="04">Apr (04)</option>
                        <option value="05">May (05)</option>
                        <option value="06">June (06)</option>
                        <option value="07">July (07)</option>
                        <option value="08">Aug (08)</option>
                        <option value="09">Sep (09)</option>
                        <option value="10">Oct (10)</option>
                        <option value="11">Nov (11)</option>
                        <option value="12">Dec (12)</option>
                      </select>
                    </div>
                    <div class="col-xs-4">
                      <select class="form-control" name="expiry-year">
                        <option value="13">2013</option>
                        <option value="14">2014</option>
                        <option value="15">2015</option>
                        <option value="16">2016</option>
                        <option value="17">2017</option>
                        <option value="18">2018</option>
                        <option value="19">2019</option>
                        <option value="20">2020</option>
                        <option value="21">2021</option>
                        <option value="22">2022</option>
                        <option value="23">2023</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control"  placeholder="Message" rows="4"></textarea>
                </div>
                <button class="btn btn-default">Submit</button>
            </form>
        </div>
        </div>
        </div>
        </div>
        <!-- reservation-information -->






        <div class="row">
          <div class="medium-6 columns medium-push-6">
            <img class="thumbnail" src="image/Photo-le-grand-hotel-Cannes.jpg">
          </div>
          <div class="medium-6 columns medium-pull-6">
            <h2>Notre hôtel,</h2>
            <p>Situé dans le centre-ville de Tours, entre la gare et le centre des congrès Vinci, l’établissement Le Grand Hotel datant du début du XXe siècle propose des chambres de style Art déco. Il dispose d'une réception ouverte 24h/24 et d'un bureau d'excursions.

Les chambres climatisées de l'établissement Le Grand Hotel sont toutes équipées d'une télévision par satellite et d'une connexion Wi-Fi gratuite. Certaines d'entre elles offrent une vue sur la gare et sa façade de style Beaux-Arts.

Au Grand Hotel, un petit-déjeuner buffet est servi tous les matins. Vous pourrez également déguster des vins régionaux au bar de l'hôtel.

La cathédrale Saint-Gatien et le Vieux-Tours sont accessibles en 8 et 15 minutes de marche respectivement. Un parking public est disponible à proximité et vous trouverez également un espace d'arrêt minute devant l'hôtel.

C'est le quartier préféré des voyageurs visitant Tours, selon les commentaires clients indépendants.

Les couples apprécient particulièrement l'emplacement de cet établissement. Ils lui donnent la note de 8,5 pour un séjour à deux..</p>
          </div>
        <hr>
        <div class="row column">
          <h3>Our Recent Work</h3>
        </div>

        <div class="row medium-up-3 large-up-4">
          <div class="column">
            <img class="thumbnail" src="image/166452812.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177400959.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177401117.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177401152.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402018.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402905.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402913.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402919.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402928.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402938.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402947.jpg">
          </div>
          <div class="column">
            <img class="thumbnail" src="image/177402962.jpg">
          </div>
        </div>

        <hr>

        <div class="row column">
          <ul class="menu">
            <li><a href="#">Haut de page</a></li>
          </ul>
        </div>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
        <script>
          $(document).foundation();
        </script>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
