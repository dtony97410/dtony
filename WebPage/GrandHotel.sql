-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 03 Mai 2019 à 08:55
-- Version du serveur :  10.1.38-MariaDB-0+deb9u1
-- Version de PHP :  7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Hotel`
--
CREATE DATABASE IF NOT EXISTS `Hotel` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `Hotel`;

-- --------------------------------------------------------

--
-- Structure de la table `Chambre`
--

CREATE TABLE `Chambre` (
  `id_chambre` int(10) NOT NULL,
  `numero` int(10) DEFAULT NULL,
  `std_sup_exc` int(10) DEFAULT NULL,
  `dispo` tinyint(1) DEFAULT NULL,
  `etage` int(10) DEFAULT NULL,
  `bain` tinyint(1) DEFAULT NULL,
  `douche` tinyint(1) DEFAULT NULL,
  `wc` tinyint(1) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `nb_lits` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Chambre`
--

INSERT INTO `Chambre` (`id_chambre`, `numero`, `std_sup_exc`, `dispo`, `etage`, `bain`, `douche`, `wc`, `tel`, `photo`, `nb_lits`) VALUES
(1, 200, 0, 1, 2, 1, 1, 1, '0262698745', NULL, 3),
(2, 201, 1, 1, 2, 0, 1, 1, '0262698746', NULL, 2),
(3, 202, 2, 1, 2, 0, 1, 0, '0262698747', NULL, 4),
(4, 203, 0, 1, 2, 1, 1, 1, '0262698748', NULL, 3),
(5, 204, 1, 1, 2, 0, 1, 1, '0262698749', NULL, 2),
(6, 205, 2, 1, 2, 0, 1, 0, '0262698750', NULL, 1),
(7, 206, 2, 1, 2, 1, 1, 1, '0262698751', NULL, 4),
(8, 207, 1, 1, 2, 0, 1, 1, '0262698752', NULL, 4),
(9, 208, 2, 1, 2, 0, 1, 0, '0262698753', NULL, 8),
(10, 209, 0, 1, 2, 1, 1, 1, '0262698754', NULL, 3),
(11, 210, 1, 1, 2, 0, 1, 1, '0262698755', NULL, 2),
(12, 211, 2, 1, 2, 0, 1, 0, '0262698756', NULL, 1),
(13, 212, 0, 1, 2, 1, 1, 1, '0262698757', NULL, 3),
(14, 213, 1, 1, 2, 0, 1, 1, '0262698758', NULL, 2),
(15, 214, 2, 1, 2, 0, 1, 0, '0262698759', NULL, 1),
(16, 215, 3, 1, 2, 1, 1, 1, '0262698760', NULL, 3),
(17, 216, 1, 1, 2, 0, 1, 1, '0262698761', NULL, 2),
(18, 217, 2, 1, 2, 0, 1, 0, '0262698762', NULL, 1),
(19, 218, 0, 1, 2, 1, 1, 1, '0262698763', NULL, 3),
(20, 219, 1, 1, 2, 0, 1, 1, '0262698764', NULL, 2),
(21, 220, 2, 1, 2, 0, 1, 0, '0262698765', NULL, 1),
(22, 221, 2, 1, 2, 1, 1, 1, '0262698766', NULL, 5),
(23, 222, 1, 1, 2, 0, 1, 1, '0262698767', NULL, 2),
(24, 223, 2, 1, 2, 0, 1, 0, '0262698768', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `Client`
--

CREATE TABLE `Client` (
  `id_client` int(10) NOT NULL,
  `nom` varchar(15) DEFAULT NULL,
  `prenom` varchar(15) DEFAULT NULL,
  `sexe` varchar(10) DEFAULT NULL,
  `adr_rue` varchar(250) DEFAULT NULL,
  `adr_cp` varchar(5) DEFAULT NULL,
  `adr_ville` varchar(25) DEFAULT NULL,
  `tel_fixe` varchar(15) DEFAULT NULL,
  `tel_gsm` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `id_compte` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Client`
--

INSERT INTO `Client` (`id_client`, `nom`, `prenom`, `sexe`, `adr_rue`, `adr_cp`, `adr_ville`, `tel_fixe`, `tel_gsm`, `email`, `id_compte`) VALUES
(1, 'Abbott', 'Rebecca', 'Féminin', '14, rue de l\'être', '97400', 'Saint-Denis', '0262852369', '0692124578', 'rebecca.abbott@gmail.com', 1),
(2, 'Addams', 'Corina', 'Féminin', '13, rue de la folle aventure', '97425', 'Les Avirons', '0262026951', '0693569641', 'addams.corina@gmail.com', 2),
(3, 'Merill', 'Cassandra', 'Féminin', '1 rue Le Conte', '97431', 'La Plaine des Palmistes', '0262542369', '0693568952', 'cassandra.merill@gmx.com', 3),
(4, 'Huggins', 'Bianca', 'Féminin', '214, avenue du prés', '97450', 'Saint-Louis', '0262145269', '0692002564', 'bianca.huggins@yahoo.fr', 4),
(5, 'Adcock', 'Sarah', 'Féminin', '156, rue du Père La Chaise', '97480', 'Saint-Joseph', '0262988874', '0692036201', 'sarah.abbott@gmail.com', 5),
(6, 'Robinson', 'Nassima', 'Féminin', '13, rue de la folle aventure', '97425', 'Les Avirons', '0262026951', '0693569641', 'robinson.nassima@gmail.com', 6);

-- --------------------------------------------------------

--
-- Structure de la table `Compte`
--

CREATE TABLE `Compte` (
  `id_compte` int(10) NOT NULL,
  `speudo` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `id_type` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Compte`
--

INSERT INTO `Compte` (`id_compte`, `speudo`, `password`, `id_type`) VALUES
(1, 'arebecca', 'arebecca', 0),
(2, 'acorina', 'acorina', 0),
(3, 'mcassandra', 'mcassandra', 0),
(4, 'hbianca', 'hbianca', 0),
(5, 'asarah', 'asarah', 0),
(6, 'rnassima', 'rnassima', 0),
(7, 'admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Structure de la table `Facture`
--

CREATE TABLE `Facture` (
  `id_facture` int(10) NOT NULL,
  `date_Facture` date DEFAULT NULL,
  `mode_paiement` varchar(15) DEFAULT NULL,
  `paiement_ok` int(10) DEFAULT NULL,
  `tva` decimal(15,0) DEFAULT NULL,
  `remise_pourcent` decimal(10,0) DEFAULT NULL,
  `id_client` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Lignefacture`
--

CREATE TABLE `Lignefacture` (
  `id_lignefacture` int(10) NOT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `total_nbnuit` int(10) DEFAULT NULL,
  `total_prix_std_sup_exc` decimal(10,0) DEFAULT NULL,
  `total_prix_sup_petitdej` float DEFAULT NULL,
  `total_saison` tinyint(10) DEFAULT NULL,
  `total_prix_sup_enfant` float DEFAULT NULL,
  `id_facture` int(10) DEFAULT NULL,
  `id_tarif` int(10) DEFAULT NULL,
  `id_reservation` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `Reservation`
--

CREATE TABLE `Reservation` (
  `id_reservation` int(10) NOT NULL,
  `date_resa` date DEFAULT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `resa_std_sup_exc` int(10) DEFAULT NULL,
  `resa_sup_petitdej` tinyint(1) DEFAULT NULL,
  `resa_nb_adulte` int(3) DEFAULT NULL,
  `resa_nb_enfant` int(3) DEFAULT NULL,
  `id_client` int(10) DEFAULT NULL,
  `id_chambre` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Reservation`
--

INSERT INTO `Reservation` (`id_reservation`, `date_resa`, `date_debut`, `date_fin`, `resa_std_sup_exc`, `resa_sup_petitdej`, `resa_nb_adulte`, `resa_nb_enfant`, `id_client`, `id_chambre`) VALUES
(1, '2018-08-10', '2018-08-10', '2018-08-20', 0, 1, 1, 0, 1, NULL),
(2, '2018-08-10', '2018-08-21', '2018-08-28', 0, 1, 1, 1, 2, NULL),
(3, '2018-08-10', '2018-08-15', '2018-08-19', 2, 1, 2, 3, 3, NULL),
(4, '2018-03-07', '2018-03-07', '2018-04-02', 0, 1, 4, 2, 4, NULL),
(5, '2018-03-07', '2018-03-07', '2018-04-02', 2, 1, 7, 2, 5, NULL),
(6, '2018-03-07', '2018-03-07', '2018-04-02', 1, 1, 1, 0, 6, NULL),
(7, '2018-01-29', '2018-01-29', '2018-01-31', 2, 1, 2, 3, 1, NULL),
(8, '2018-01-29', '2018-01-29', '2018-06-10', 0, 1, 2, 1, 2, NULL),
(9, '2018-07-10', '2018-07-10', '2018-07-25', 0, 0, 2, 2, 3, NULL),
(10, '2018-03-08', '2018-03-08', '2018-05-08', 0, 0, 2, 2, 4, NULL),
(11, '2018-01-15', '2018-01-15', '2018-02-01', 0, 1, 1, 2, 5, NULL),
(12, '2018-02-01', '2018-02-01', '2018-03-04', 0, 1, 1, 1, 6, NULL),
(13, '2018-01-29', '2018-01-29', '2018-06-10', 1, 1, 1, 3, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Tarif`
--

CREATE TABLE `Tarif` (
  `id_tarif` int(10) NOT NULL,
  `date_debut` date DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `prix_std` decimal(10,0) DEFAULT NULL,
  `prix_sup` decimal(10,0) DEFAULT NULL,
  `prix_exc` decimal(10,0) DEFAULT NULL,
  `prix_sup_petitdej` decimal(10,0) DEFAULT NULL,
  `saison` tinyint(1) DEFAULT NULL,
  `prix_sup_enfant` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Tarif`
--

INSERT INTO `Tarif` (`id_tarif`, `date_debut`, `date_fin`, `prix_std`, `prix_sup`, `prix_exc`, `prix_sup_petitdej`, `saison`, `prix_sup_enfant`) VALUES
(1, '2018-01-01', '2018-01-28', '10', '15', '20', '5', 0, NULL),
(2, '2018-01-28', '2018-02-02', '20', '25', '30', '5', 1, NULL),
(3, '2018-02-02', '2018-02-05', '10', '15', '20', '5', 0, NULL),
(4, '2018-02-05', '2018-03-05', '20', '25', '30', '5', 1, NULL),
(5, '2018-03-05', '2018-04-07', '10', '15', '20', '5', 0, NULL),
(6, '2018-04-07', '2018-07-12', '20', '25', '30', '5', 1, NULL),
(7, '2018-07-12', '2018-12-31', '10', '15', '20', '5', 0, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Chambre`
--
ALTER TABLE `Chambre`
  ADD PRIMARY KEY (`id_chambre`);

--
-- Index pour la table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `FK_Client_id_compte` (`id_compte`);

--
-- Index pour la table `Compte`
--
ALTER TABLE `Compte`
  ADD PRIMARY KEY (`id_compte`);

--
-- Index pour la table `Facture`
--
ALTER TABLE `Facture`
  ADD PRIMARY KEY (`id_facture`),
  ADD KEY `FK_Facture_id_client` (`id_client`);

--
-- Index pour la table `Lignefacture`
--
ALTER TABLE `Lignefacture`
  ADD PRIMARY KEY (`id_lignefacture`),
  ADD KEY `FK_Lignefacture_id_facture` (`id_facture`),
  ADD KEY `FK_Lignefacture_id_tarif` (`id_tarif`),
  ADD KEY `FK_Lignefacture_id_reservation` (`id_reservation`);

--
-- Index pour la table `Reservation`
--
ALTER TABLE `Reservation`
  ADD PRIMARY KEY (`id_reservation`),
  ADD KEY `FK_Reservation_id_client` (`id_client`),
  ADD KEY `FK_Reservation_id_chambre` (`id_chambre`);

--
-- Index pour la table `Tarif`
--
ALTER TABLE `Tarif`
  ADD PRIMARY KEY (`id_tarif`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Chambre`
--
ALTER TABLE `Chambre`
  MODIFY `id_chambre` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `Client`
--
ALTER TABLE `Client`
  MODIFY `id_client` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `Compte`
--
ALTER TABLE `Compte`
  MODIFY `id_compte` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `Facture`
--
ALTER TABLE `Facture`
  MODIFY `id_facture` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Lignefacture`
--
ALTER TABLE `Lignefacture`
  MODIFY `id_lignefacture` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Reservation`
--
ALTER TABLE `Reservation`
  MODIFY `id_reservation` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `Tarif`
--
ALTER TABLE `Tarif`
  MODIFY `id_tarif` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Client`
--
ALTER TABLE `Client`
  ADD CONSTRAINT `FK_Client_id_compte` FOREIGN KEY (`id_compte`) REFERENCES `Compte` (`id_compte`);

--
-- Contraintes pour la table `Facture`
--
ALTER TABLE `Facture`
  ADD CONSTRAINT `FK_Facture_id_client` FOREIGN KEY (`id_client`) REFERENCES `Client` (`id_client`);

--
-- Contraintes pour la table `Lignefacture`
--
ALTER TABLE `Lignefacture`
  ADD CONSTRAINT `FK_Lignefacture_id_facture` FOREIGN KEY (`id_facture`) REFERENCES `Facture` (`id_facture`),
  ADD CONSTRAINT `FK_Lignefacture_id_reservation` FOREIGN KEY (`id_reservation`) REFERENCES `Reservation` (`id_reservation`),
  ADD CONSTRAINT `FK_Lignefacture_id_tarif` FOREIGN KEY (`id_tarif`) REFERENCES `Tarif` (`id_tarif`);

--
-- Contraintes pour la table `Reservation`
--
ALTER TABLE `Reservation`
  ADD CONSTRAINT `FK_Reservation_id_chambre` FOREIGN KEY (`id_chambre`) REFERENCES `Chambre` (`id_chambre`),
  ADD CONSTRAINT `FK_Reservation_id_client` FOREIGN KEY (`id_client`) REFERENCES `Client` (`id_client`);
--
-- Base de données :  `eleve1_db`
--
CREATE DATABASE IF NOT EXISTS `eleve1_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `eleve1_db`;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `age` int(3) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `age`, `location`, `date`) VALUES
(1, 'testa', 'testb', 'test@test.com', 25, 'Ici', '2018-11-16 13:41:58'),
(2, 'Eleve1', 'Prénom de l\'élève1', 'eleve1@gmail.com', 23, 'Perros GUIRREC', '2018-11-16 18:53:01');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;--
-- Base de données :  `eleve2_db`
--
CREATE DATABASE IF NOT EXISTS `eleve2_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `eleve2_db`;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `age` int(3) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Base de données :  `eleve3_db`
--
CREATE DATABASE IF NOT EXISTS `eleve3_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `eleve3_db`;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `age` int(3) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Base de données :  `eleve4_db`
--
CREATE DATABASE IF NOT EXISTS `eleve4_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `eleve4_db`;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `age` int(3) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Base de données :  `eleve5_db`
--
CREATE DATABASE IF NOT EXISTS `eleve5_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `eleve5_db`;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `age` int(3) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Base de données :  `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Structure de la table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Structure de la table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Structure de la table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Structure de la table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Structure de la table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Structure de la table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Structure de la table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Contenu de la table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('eleve1', '[{\"db\":\"Hotel\",\"table\":\"Chambre\"},{\"db\":\"eleve1_db\",\"table\":\"Chambre\"},{\"db\":\"eleve1_db\",\"table\":\"users\"}]');

-- --------------------------------------------------------

--
-- Structure de la table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Structure de la table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Structure de la table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Structure de la table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Contenu de la table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('eleve1', '2018-11-15 17:21:28', '{\"collation_connection\":\"utf8mb4_unicode_ci\",\"lang\":\"fr\"}'),
('eleve2', '2018-11-16 18:58:03', '{\"collation_connection\":\"utf8mb4_unicode_ci\",\"lang\":\"fr\"}'),
('eleve3', '2018-11-16 19:05:15', '{\"collation_connection\":\"utf8mb4_unicode_ci\",\"lang\":\"fr\"}'),
('eleve4', '2018-11-16 19:10:32', '{\"lang\":\"fr\",\"collation_connection\":\"utf8mb4_unicode_ci\"}'),
('eleve5', '2018-11-16 19:15:35', '{\"lang\":\"fr\",\"collation_connection\":\"utf8mb4_unicode_ci\"}'),
('phpmyadmin', '2018-11-15 17:01:50', '{\"collation_connection\":\"utf8mb4_unicode_ci\"}');

-- --------------------------------------------------------

--
-- Structure de la table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Structure de la table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Index pour les tables exportées
--

--
-- Index pour la table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Index pour la table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Index pour la table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Index pour la table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Index pour la table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Index pour la table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Index pour la table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Index pour la table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Index pour la table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Index pour la table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Index pour la table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Index pour la table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Index pour la table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Index pour la table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Index pour la table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
