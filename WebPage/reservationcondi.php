<!--CONNEXION A LA BASE DE DONNEE---->
<?php
try
{
    $bdd = new PDO('mysql:host=localhost;dbname=hotel;charset=utf8', 'root', '');
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
/////////////////////////////////////////////////////////////////////
//Recuperer les variables post------>

$date_debut = $_POST['date_debut'];
$date_fin = $_POST['date_fin'];
$std_sup_exc = $_POST['std_sup_exc'];
$total_nbnuit = $_POST['total_nbnuit'];

//////////////////////////////////////////////////////////////////////////
dx = $date_debut
dy = $date_fin
myArrayOf_Id_tarif = [[[R1 retourne un tableau contenant tous les id_tarif qui sont concernés par la période de réservation de l'internaute. Un chevauchement sur l'année suivante est possible, mais uniquement une année. En d'autre terme, il n'est pas possible de réserver plus de 2 ans moins 1 jour. Attention, ce tableau est parcouru en prenant la dernière période en premier. ]]]
POUR (toutes la valeurs du tableau myArrayOf_Id_tarif) FAIRE
 d0 = [[[R2 date de début d'une période de la table 'Tarif' (pour l'id_tarif courant) ]]]
 d1 =[[[R3 date de fin d'une période de la table 'Tarif' (pour l'id_tarif courant) ]]]
 total_prix_std_sup_exc = [[[R4 le tarif de la chambre pour la période considérée (donc pour l'id_tarif courant) ]]]
 total_prix_sup_lits = [[[R5 (le tarif du supplément lits pour la période considérée (donc pour l'id_tarif courant)) ]]]
 total_prix_sup_enfant = [[[R6 (le tarif du supplément enfant pour la période considérée (donc pour l'id_tarif courant)) * resa_nb_enfant ]]]
 total_prix_sup_petitdej = [[[R7 le tarif du supplément petit déjeuner pour la période considérée (donc pour l'id_tarif courant) ]]]
    total_saison = [[[R8 l'information en saison ou hors saison ]]]
     if (dx != dy) do
       if (dx >= d0) do
        nbnuit1 = dy - dx;

           total_prix_std_sup_exc = total_prix_std_sup_exc * nbnuit1;
           total_prix_sup_enfant = total_prix_sup_enfant * nbnuit1 * nb_enfant;
           total_prix_sup_petitdej = total_prix_sup_petitdej * nbnuit1 * nb_adulte;
           total_saison = total_saison * nbnuit1;

[[[R9 INSERER les valeurs total_prix_std_sup_exc, total_prix_sup_enfant, total_prix_sup_petitdej et total_saison dans la table `Lignefacture` ]]]

// On se déplace dans l'interval de date en réduisant la période à examiner.            dy = dx;

     else
         if (dy >= d0) ALORS
            nbnuit2 = dy - d0;

             total_prix_std_sup_exc = total_prix_std_sup_exc * nbnuit2;
             total_prix_sup_enfant = total_prix_sup_enfant * nbnuit2 * nb_enfant;
             total_prix_sup_petitdej = total_prix_sup_petitdej * nbnuit2 * nb_adulte;
             total_saison = total_saison * nbnuit2;

             [[[R10 INSERER les valeurs total_prix_std_sup_exc, total_prix_sup_enfant, total_prix_sup_petitdej et total_saison dans la table `Lignefacture` ]]]

             // On se déplace dans l'interval de date en réduisant la période à examiner.                dy = d0;
             dy = d0;
     FIN SI
  FIN SI
FIN SI FIN POUR
